#!/bin/bash
number_to_char() {
    local n="$1"
    local result=""
    while [ "$n" -gt 0 ]; do
        char=$(( (n - 1) % 26 + 97 ))
        result="$(printf \\$(printf '%03o' "$char"))$result"
        n=$(( (n - 1) / 26 ))
    done
    echo "$result"
}
touch tt.txt
link_count=$(cat tt.txt)
last_redirect_url=""
touch kq.txt
while true; do
    link_count=$((link_count + 1))
    chars=$(number_to_char "$link_count")
    url="https://s.imap.edu.vn/ie$chars"
    redirect_url=$(curl -I -s "$url" | grep -i "Location" | awk '{print $2}')

    if [[ "$redirect_url" != "https://s.imap.edu.vn" && "$redirect_url" != "$last_redirect_url" ]]; then
        echo "$url;$redirect_url" >> kq.txt
        last_redirect_url="$redirect_url"
    fi
    
    echo "$link_count" > tt.txt
done

